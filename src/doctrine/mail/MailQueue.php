<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Doctrine\Mail;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Mailing\Model\MailParameterValue;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
class MailQueue
{
    public const STATUS_NEW         = 0;
    public const STATUS_IN_PROGRESS = 10;
    public const STATUS_SENT        = 20;
    public const STATUS_ERROR       = 30;
    use Entity\Id;
    use Entity\Status;

    #[ORM\Column(type: Types::TEXT)]
    private string $content;

    #[ORM\Column]
    private string $subject = '';

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private DateTimeInterface $createdAt;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $sentAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $errorAt = null;

    #[ORM\Column]
    private string $error = '';

    /** @var mixed[] */
    #[ORM\Column(type: Types::ARRAY)]
    private array $recipients;

    /** @var mixed[] */
    #[ORM\Column(type: Types::ARRAY)]
    private array $parameters = [];

    #[ORM\Column(type: Types::TEXT)]
    private string $templateSerialize = '';

    #[ORM\ManyToOne(targetEntity: MailTemplate::class, cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?MailTemplate $template = null;

    public function __construct()
    {
        $this->status    = self::STATUS_NEW;
        $this->createdAt = new DateTime();
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function getSentAt(): ?DateTimeInterface
    {
        return $this->sentAt;
    }

    public function getErrorAt(): ?DateTimeInterface
    {
        return $this->errorAt;
    }

    public function getError(): string
    {
        return $this->error;
    }

    /**
     * @return string[]
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @return MailParameterValue[]
     */
    public function getParameters(): array
    {
        $parameters = [];
        foreach ($this->parameters as $parameter) {
            $parameters[] = new MailParameterValue($parameter['name'], $parameter['value']);
        }

        return $parameters;
    }

    /**
     * @param string[]             $recipients
     * @param MailParameterValue[] $parameterValues
     */
    public function create(MailTemplate $template, array $recipients, array $parameterValues, string $content, string $subject): void
    {
        $this->template   = $template;
        $this->recipients = $recipients;
        $this->content    = $content;
        $this->subject    = $subject;

        $parameters = [];
        foreach ($parameterValues as $parameterValue) {
            $parameters[] = $parameterValue->getDataForSerialize();
        }

        $this->parameters = $parameters;
    }

    /**
     * @param string[]             $recipients
     * @param MailParameterValue[] $parameterValues
     */
    public function createByTemplateSerialize(string $templateSerialize, array $recipients, array $parameterValues, string $content, string $subject): void
    {
        $this->templateSerialize = $templateSerialize;
        $this->recipients        = $recipients;
        $this->content           = $content;
        $this->subject           = $subject;

        $parameters = [];
        foreach ($parameterValues as $parameterValue) {
            $parameters[] = $parameterValue->getDataForSerialize();
        }

        $this->parameters = $parameters;
    }

    public function setStatusSent(): void
    {
        $this->status  = self::STATUS_SENT;
        $this->sentAt  = new DateTime();
        $this->errorAt = null;
        $this->error   = '';
    }

    public function setStatusInProgress(): void
    {
        $this->status = self::STATUS_IN_PROGRESS;
    }

    public function setStatusError(string $error): void
    {
        $this->status  = self::STATUS_ERROR;
        $this->errorAt = new DateTime();
        $this->error   = $error;
    }

    public function getTemplate(): ?MailTemplate
    {
        return $this->template;
    }

    public function getTemplateSerialize(): string
    {
        return $this->templateSerialize;
    }
}
