<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Skadmin\FileStorage\FilePreview;
use Ublaboo\Mailing\IMessageData;

use function in_array;

final class CMailMessage implements IMessageData
{
    private string $subject;
    private string $content;
    private string $preheader;

    /** @var string[] */
    private array $recipients;

    /** @var string[] */
    private array $systemRecipients;

    /** @var FilePreview[] */
    private array $attachments;

    /**
     * @param string[]      $recipients
     * @param string[]      $systemRecipients
     * @param FilePreview[] $attachments
     */
    public function __construct(string $subject, string $content, string $preheader, array $recipients, array $systemRecipients, array $attachments = [])
    {
        $this->subject          = $subject;
        $this->content          = $content;
        $this->preheader        = $preheader;
        $this->recipients       = $recipients;
        $this->systemRecipients = $systemRecipients;
        $this->attachments      = $attachments;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getPreheader(): string
    {
        return $this->preheader;
    }

    public function addRecipient(string $recipient): void
    {
        if (in_array($recipient, $this->recipients)) {
            return;
        }

        $this->recipients[] = $recipient;
    }

    /**
     * @return string[]
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @return string[]
     */
    public function getSystemRecipients(): array
    {
        return $this->systemRecipients;
    }

    /**
     * @return FilePreview[]
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param FilePreview[] $attachments
     */
    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }
}
