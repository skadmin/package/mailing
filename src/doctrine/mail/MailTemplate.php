<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Doctrine\Mail;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Mailing\Model\MailTemplateParameter;
use SkadminUtils\DoctrineTraits\Entity;

use function implode;
use function is_bool;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MailTemplate implements IMailTemplate
{
    use Entity\Id;
    use Entity\Name;
    use Entity\LastUpdate;

    #[ORM\Column]
    private string $subject = '';

    #[ORM\Column(type: Types::ARRAY, options: ['default' => ''])]
    /** @var array<string>|bool */
    private array|bool $recipients = [];

    #[ORM\Column(options: ['default' => ''])]
    private string $preheader = '';

    #[ORM\Column(type: Types::TEXT)]
    private string $content = '';

    #[ORM\Column]
    private string $type = '';

    #[ORM\Column]
    private string $class = '';

    #[ORM\Column(type: Types::ARRAY)]
    /** @var mixed[] */
    private array $parameters = [];

    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @return array<string>|string
     */
    public function getRecipients(?string $implodeBy = null): array|string
    {
        $recipients = is_bool($this->recipients) ? [] : $this->recipients;

        if ($implodeBy === null) {
            return $recipients;
        }

        return implode($implodeBy, $recipients);
    }

    public function getPreheader(): string
    {
        return $this->preheader;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @return MailTemplateParameter[]
     */
    public function getParameters(): array
    {
        $parameters = [];
        foreach ($this->parameters as $parameter) {
            $name         = $parameter['name'];
            $description  = $parameter['description'];
            $example      = $parameter['example'];
            $parameters[] = new MailTemplateParameter($name, $description, $example);
        }

        return $parameters;
    }

    /**
     * @param array<string> $recipients
     */
    public function update(string $subject, array $recipients, string $preheader, string $content): void
    {
        $this->subject    = $subject;
        $this->recipients = $recipients;
        $this->preheader  = $preheader;
        $this->content    = $content;
    }
}
