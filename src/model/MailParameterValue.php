<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use Nette\SmartObject;

class MailParameterValue
{
    use SmartObject;

    private string $name;
    private mixed  $value;

    public function __construct(string $name, mixed $value)
    {
        $this->name  = $name;
        $this->value = $value;
    }

    /**
     * @return string[]
     */
    public function getDataForSerialize(): array
    {
        return [
            'name'  => $this->getName(),
            'value' => $this->getValue(),
        ];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}
