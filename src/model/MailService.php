<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use DateTimeInterface;
use Nette\Utils\Arrays;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\FileStorage\FilePreview;
use Skadmin\FileStorage\FileStorage;
use Skadmin\Mailing\Doctrine\Mail\IMailTemplate;
use Skadmin\Mailing\Doctrine\Mail\MailFacade;
use Skadmin\Mailing\Doctrine\Mail\MailQueue;
use Skadmin\Mailing\Doctrine\Mail\MailTemplate;
use Skadmin\Translator\Translator;
use SkadminUtils\Utils\Utils\Strings;
use Throwable;
use Ublaboo\Mailing\MailFactory;

use function assert;
use function explode;
use function method_exists;
use function sprintf;
use function trim;
use function unserialize;

class MailService
{
    private EntityManagerDecorator $entityManager;
    private MailFactory            $mailFactory;
    private MailFacade             $facade;
    private FileStorage            $fileStorage;
    private Translator             $translator;

    public function __construct(EntityManagerDecorator $entityManager, MailFactory $mailFactory, MailFacade $facade, FileStorage $fileStorage, Translator $translator)
    {
        $this->entityManager = $entityManager;
        $this->mailFactory   = $mailFactory;
        $this->facade        = $facade;
        $this->fileStorage   = $fileStorage;
        $this->translator    = $translator;
    }

    /**
     * @param string[] $recipients
     */
    public function addByTemplateType(string $type, CMail $cMail, array $recipients, bool $send = false): ?MailQueue
    {
        return $this->add($this->getTemplateByType($type), $cMail, $recipients, $send);
    }

    /**
     * @param string[] $recipients
     */
    public function add(IMailTemplate $template, CMail $cMail, array $recipients, bool $send = false): ?MailQueue
    {
        $functionMatch = static function (array $m) use ($cMail): string {
            $match  = trim($m[0], '[]');
            $method = sprintf('get%s', Strings::camelize($match));

            if (method_exists($cMail, $method)) {
                $value = $cMail->$method();

                if ($value instanceof DateTimeInterface) {
                    return $value->format('d.m.Y H:i');
                }

                return (string) $value;
            }

            return $match;
        };

        $content = Strings::replace($template->getContent(), '~\[[a-z\-]+\]~i', $functionMatch);
        $subject = Strings::replace($template->getSubject(), '~\[[a-z\-]+\]~i', $functionMatch);

        $mailQueue = $this->facade->createMailQueue($template, $cMail, $recipients, $content, $subject);

        if ($send) {
            $mailQueue = $this->send($mailQueue);
        }

        return $mailQueue;
    }

    public function resendFailedEmails(): void
    {
        foreach ($this->facade->findFailedEmails() as $mailQueue) {
            $this->send($mailQueue);
        }
    }

    public function send(MailQueue $mailQueue): ?MailQueue
    {
        $this->facade->prepareMail($mailQueue);
        if (! $mailQueue->isLoaded()) {
            return null;
        }

        $mailTemplate = $mailQueue->getTemplate();
        if (! $mailTemplate instanceof IMailTemplate) {
            $mailTemplate = unserialize($mailQueue->getTemplateSerialize());

            if (method_exists($mailTemplate, 'getFilesIdentifiers')) {
                $templateRepository = $this->entityManager->getRepository($mailTemplate::class);
                $mailTemplate       = $templateRepository->find($mailTemplate->getId());
            }
        }

        $cMail = new CMailMessage($mailQueue->getSubject(), $mailQueue->getContent(), $mailTemplate->getPreheader(), $mailQueue->getRecipients(), $mailTemplate->getRecipients());

        if (method_exists($mailTemplate, 'getFilesIdentifiers')) {
            $attachments = Arrays::map($mailTemplate->getFilesIdentifiers(), fn (string $fileIdentifier): FilePreview => $this->fileStorage->getFilePreview($fileIdentifier));
        } else {
            $attachments = [];
        }

        foreach ($mailQueue->getParameters() as $parameter) {
            if ($parameter->getName() === 'attachments') {
                foreach (explode(';', $parameter->getValue()) as $fileIdentifier) {
                    if ($fileIdentifier === '') {
                        continue;
                    }

                    $attachments[] = $this->fileStorage->getFilePreview($fileIdentifier);
                }
            } elseif ($parameter->getName() === 'email-to') {
                if (trim($parameter->getValue()) !== '') {
                    $cMail->addRecipient($parameter->getValue());
                }
            }
        }

        $cMail->setAttachments($attachments);

        try {
            $this->mailFactory->createByType('Skadmin\Mailing\Model\MailMessage', $cMail)
                ->send();
            $mailQueue = $this->facade->sendMail($mailQueue);
        } catch (Throwable $e) {
            $mailQueue = $this->facade->failMail($mailQueue, $e->getMessage());
        }

        return $mailQueue;
    }

    public function getTemplateByType(string $type): MailTemplate
    {
        $mailTemplate = $this->facade->findByType($type);
        assert($mailTemplate instanceof MailTemplate);

        return $mailTemplate;
    }

    public function sendByMailQueueId(int $id): ?MailQueue
    {
        return $this->send($this->facade->getQueue($id));
    }
}
