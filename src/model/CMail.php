<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Model;

use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use SkadminUtils\Utils\Utils\Strings;

use function assert;

abstract class CMail
{
    /**
     * @return mixed[]
     */
    abstract public static function getModelForSerialize(): array;

    /**
     * @return MailParameterValue[]
     */
    abstract public function getParameterValues(): array;

    /**
     * @return string[]
     *
     * @throws ReflectionException
     */
    protected static function getProperties(string $class): array
    {
        $reflection  = new ReflectionClass($class);
        $_properties = $reflection->getProperties(ReflectionProperty::IS_PRIVATE);

        $properties = [];

        foreach ($_properties as $property) {
            assert($property instanceof ReflectionProperty);
            if ($property->getDeclaringClass()->getName() !== $class) {
                continue;
            }

            $properties[] = Strings::camelizeToWebalize($property->getName());
        }

        return $properties;
    }
}
