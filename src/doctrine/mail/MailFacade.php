<?php

declare(strict_types=1);

namespace Skadmin\Mailing\Doctrine\Mail;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Mailing\Model\CMail;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;
use function serialize;

final class MailFacade extends Facade
{
    private string $tableQueue;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);

        $this->table      = MailTemplate::class;
        $this->tableQueue = MailQueue::class;
    }

    /**
     * @param array<string> $recipients
     */
    public function updateTemplateById(int $id, string $subject, array $recipients, string $preheader, string $content): ?MailTemplate
    {
        return $this->updateTemplate($this->getTemplate($id), $subject, $recipients, $preheader, $content);
    }

    /**
     * @param array<string> $recipients
     */
    public function updateTemplate(MailTemplate $mailTemplate, string $subject, array $recipients, string $preheader, string $content): MailTemplate
    {
        $mailTemplate->update($subject, $recipients, $preheader, $content);

        $this->em->flush();

        return $mailTemplate;
    }

    public function getTemplate(int $id): MailTemplate
    {
        return parent::getFrom($id, $this->table);
    }

    public function findByType(string $type): ?MailTemplate
    {
        $criteria = ['type' => $type];

        $mailTemplate = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
        assert($mailTemplate instanceof MailTemplate || $mailTemplate === null);

        return $mailTemplate;
    }

    /**
     * @return MailTemplate[]
     */
    public function getAllTemplate(): array
    {
        return $this->em
            ->getRepository($this->table)
            ->findAll();
    }

    /**
     * @param string[] $recipients
     */
    public function createMailQueue(IMailTemplate $template, CMail $cMail, array $recipients, string $content, string $subject): MailQueue
    {
        $mailQueue = $this->getQueue();

        if ($template::class === MailTemplate::class) {
            $mailQueue->create($template, $recipients, $cMail->getParameterValues(), $content, $subject);
        } else {
            $templateSerialize = serialize($template);
            $mailQueue->createByTemplateSerialize($templateSerialize, $recipients, $cMail->getParameterValues(), $content, $subject);
        }

        $this->em->persist($mailQueue);
        $this->em->flush();

        return $mailQueue;
    }

    public function getQueue(?int $id = null): MailQueue
    {
        if ($id === null) {
            return new MailQueue();
        }

        $mailQueue = parent::getFrom($id, $this->tableQueue);

        return $mailQueue ?? new MailQueue();
    }

    public function sendMail(MailQueue $mailQueue): MailQueue
    {
        $mailQueue->setStatusSent();
        $this->em->flush();

        return $mailQueue;
    }

    public function prepareMail(MailQueue $mailQueue): MailQueue
    {
        $mailQueue->setStatusInProgress();
        $this->em->flush();

        return $mailQueue;
    }

    public function failMail(MailQueue $mailQueue, string $error): MailQueue
    {
        $mailQueue->setStatusError($error);
        $this->em->flush();

        return $mailQueue;
    }

    /**
     * @return array<MailQueue>
     */
    public function findFailedEmails(): array
    {
        $repository = $this->em->getRepository($this->tableQueue);
        assert($repository instanceof EntityRepository);

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->neq('a.errorAt', null));

        return $repository->createQueryBuilder('a')
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();
    }
}
